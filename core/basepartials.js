module.exports = {
    HEAD_PARTIAL: './partials/htmlbase/head',
    HEADER_PARTIAL: './partials/htmlbase/header',
    FOOTER_PARTIAL: './partials/htmlbase/footer',
    SIDEBAR_PARTIAL: './partials/htmlbase/sidebar',
    JSINCLUDES_PARTIAL: './partials/jsincludes/base'
}