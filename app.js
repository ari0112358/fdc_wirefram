'use strict'

// Includes
const express = require('express')

// Web route include
const webroutes = require('./routes/web')

//Constants
const APP_TITLE = 'FDC'
const PORT = 5000
const syncStatus = false;


const app = express()

// Ejs view engine
app.set('view engine', 'ejs')


// Static Paths
app.use(express.static('static'))
app.use('/css', express.static(__dirname + 'static/css'))
app.use('/js', express.static(__dirname + 'static/js'))

webroutes(app)

var server = app.listen(PORT, function () {
  var port = server.address().port
  console.log(APP_TITLE + ' server started at port ' + port)
})
