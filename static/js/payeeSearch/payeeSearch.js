const statusValues = [
    "Available for use",
    "Reassigned a new id",
    "Unavailable for use",
    "Limited use",
    "County clerk use",
    "Centralized payee id",
    "No longer used"
]

const searchButtonId = "payeeSearch";
$(document).ready( function() {
    try{
        $(`#${searchButtonId}`).on('click',async ()=>{
            await loadPayeeTable(staticData);
        })
    } catch(error){
        console.log(error)
    }
});

const payeeTableId = "payeeTable";
const payeeModalId = "payeeModal";
const editButtonId = "editPayee";
const saveButtonId = "savePayee";
let payeeTable = null;
async function loadPayeeTable(data){
    for(let i = 0; i < staticData.length; i++){
        let row = staticData[i];
        staticData[i].id = `01${(row.name.split(' ')[1].substring(0,4)).toUpperCase()}${row.name.split(' ')[0].substring(0,1).toUpperCase()}001`;
        staticData[i].status = statusValues[Math.floor(Math.random() * 7)];
    }
    if(payeeTable != null) await payeeTable.destroy();
    payeeTable = $(`#${payeeTableId}`).DataTable({
        data: staticData,
        columns: [
            { 
                title: "Id",
                data: "id"
            },
            { 
                title: "Name",
                data: 'name'
            },
            { 
                title: "Address",
                data: 'address'
            },
            { 
                title: "City",
                data: 'city'
            },
            { 
                title: "State",
                data: 'state'
            },
            { 
                title: "Zipcode",
                data: 'zip'
            },
            {
                title: "Status",
                data: 'status'
            }
        ],
        columnDefs: [
            {
                targets: 0,
                render: function(data, type, row, meta){
                    return row.id;
                }
            }, 
            {
                /* targets: 1,
                render: function(data, type, row, meta){
                    return `<span class="clickable"><u>${row.name}</u></span>`
                } */
            },
            /* {
                targets: 6,
                render: function(data, type, row, meta){
                    return statusValues[Math.floor(Math.random() * 7)]
                }
            } */
        ],
        drawCallback: function(settings){
        }
    });
    
    $(`#${payeeTableId}`).removeClass('d-none');
    $(`#${payeeTableId} tbody`).on('click', 'tr', function(){
        let entry = payeeTable.row(this).data();
        console.log(entry);
        /* 2 digit
        4 letter lastname
        1 letter firstname
        3 digit number */
        //Need better way maybe
        $('#payeeId').val(entry.id);
        $('#payeeId_span').html(entry.id);

        $('#payeeName').val(entry.name);
        $('#payeeName_span').html(entry.name);

        $('#payeeAddress').val(entry.address);
        $('#payeeAddress_span').html(entry.address);

        $('#payeeCity').val(entry.city);
        $('#payeeCity_span').html(entry.city);

        $('#payeeState').val(entry.state);
        $('#payeeState_span').html(entry.state);

        $('#payeeZip').val(entry.zip);
        $('#payeeZip_span').html(entry.zip);

        $('#payeeStatus').val(entry.status);
        $('#payeeStatus_span').html(entry.status);
        $(`#${payeeModalId}`).modal('toggle');
    })

    $(`#${editButtonId}`).on('click', function(event){
        $('.payeeDisplay').addClass('d-none');
        $('.payeeFormInput').removeClass('d-none');
        $(`#${editButtonId}`).addClass('d-none');
        $(`#${saveButtonId}`).removeClass('d-none');
    });

    $(`#${saveButtonId}`).on('click', function(event){
        $('.payeeDisplay').removeClass('d-none');
        $('.payeeFormInput').addClass('d-none');
        $(`#${editButtonId}`).removeClass('d-none');
        $(`#${saveButtonId}`).addClass('d-none');
    });
}