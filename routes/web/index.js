'use strict'
const basepartials = require('../../core/basepartials')
const homeIncludes = require('../../core/homeIncludes')
const op20includes = require('../../core/op20includes')
const op05includes = require('../../core/op05includes')

module.exports = (app) => {
    app.get('/', async (req, res) => {
        res.render('home', {
            head: basepartials.HEAD_PARTIAL,
            header: basepartials.HEADER_PARTIAL,
            footer: basepartials.FOOTER_PARTIAL,
            jsincludes: basepartials.JSINCLUDES_PARTIAL,
            sidebar: basepartials.SIDEBAR_PARTIAL,
            flexItems: homeIncludes.flexItems,
            additionalJS: homeIncludes.additionalJS,
            additionalCSS: homeIncludes.additionalCSS,
            modalItems: homeIncludes.modalItems,
            modalNames: homeIncludes.modalNames
        })
    })

    app.get('/op20', async (req, res) => {
        res.render('op20', {
            head: basepartials.HEAD_PARTIAL,
            header: basepartials.HEADER_PARTIAL,
            footer: basepartials.FOOTER_PARTIAL,
            jsincludes: basepartials.JSINCLUDES_PARTIAL,
            sidebar: basepartials.SIDEBAR_PARTIAL,
            flexItems: op20includes.flexItems,
            additionalJS: op20includes.additionalJS,
            additionalCSS: op20includes.additionalCSS,
        })
    })
    app.get('/op05', async (req, res) => {
        res.render('op05', {
            head: basepartials.HEAD_PARTIAL,
            header: basepartials.HEADER_PARTIAL,
            footer: basepartials.FOOTER_PARTIAL,
            jsincludes: basepartials.JSINCLUDES_PARTIAL,
            sidebar: basepartials.SIDEBAR_PARTIAL,
            flexItems: op05includes.flexItems,
            additionalJS: op05includes.additionalJS,
            additionalCSS: op05includes.additionalCSS,
        })
    })

    app.get('/aboutus', async(req, res) => {
        res.render('aboutus')
    })
}